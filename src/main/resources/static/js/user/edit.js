System.config({
    meta: {
        jquery: { format: 'global' },
        bootstrap: { format: 'global', deps: ['jquery']},
        fancybox: { format: 'global', deps: ['jquery']}
    },
    paths: {
        jquery: '/js/jquery.min.js',
        bootstrap: '/js/bootstrap-3.1.1.min.js',
        fancybox: '/js/jquery.fancybox.pack.js'
    }
});
Promise.all([
    System.import('fancybox')
]).then(function(modules) {
    $('.btn-add-bonus').on('click', function (event) {
        $.fancybox('#b-add-bonus');
    });
    $('.add-bonus-submit').on('click', function() {
        console.log('add-bonus-submit-clicked');
        var token = $("input[name='_csrf']").val();
        var header = $("input[name='_csrf_header']").val();
        $(document).ajaxSend(function(e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });

        setTimeout(function() {
            var bonusAmount = $('input[name=bonusAmount]').val();
            $.ajax({
                url: '/panel/manage/user/addbonus?amount=' + bonusAmount,
                type: 'post',
                contentType: 'applicaion/json',
                success: function() {
                    $.fancybox.close();
                }
            });
        }, 1000);
    });

}, console.error.bind(console));
