package net.dm.users.event;

import net.dm.users.entity.User;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class UserCreatedEventPublisher
   implements ApplicationEventPublisherAware {
   
   private ApplicationEventPublisher publisher;

   public void setApplicationEventPublisher
              (ApplicationEventPublisher publisher){
      this.publisher = publisher;
   }

   public void publish(User user) {
      UserCreatedEvent uce = new UserCreatedEvent(this);
      uce.setUser(user);
      publisher.publishEvent(uce);
   }
}