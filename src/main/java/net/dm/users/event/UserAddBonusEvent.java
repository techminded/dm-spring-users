package net.dm.users.event;

import net.dm.users.entity.User;
import org.springframework.context.ApplicationEvent;

public class UserAddBonusEvent extends ApplicationEvent {

   private User user;

   private int amount;

   public UserAddBonusEvent(Object source) {
      super(source);
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

   public int getAmount() {
      return amount;
   }

   public void setAmount(int amount) {
      this.amount = amount;
   }

   public String toString(){

      return "User Add Bonus Event";
   }
}