package net.dm.users.event;

import net.dm.users.entity.User;
import org.springframework.context.ApplicationEvent;

public class UserCreatedEvent extends ApplicationEvent {

   private User user;
   
   public UserCreatedEvent(Object source) {
      super(source);
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

   public String toString(){

      return "User Created Event";
   }
}