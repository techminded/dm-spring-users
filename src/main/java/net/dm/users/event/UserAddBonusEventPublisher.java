package net.dm.users.event;

import net.dm.users.entity.User;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class UserAddBonusEventPublisher
   implements ApplicationEventPublisherAware {
   
   private ApplicationEventPublisher publisher;

   public void setApplicationEventPublisher
              (ApplicationEventPublisher publisher){
      this.publisher = publisher;
   }

   public void publish(User user, int amount) {
      UserAddBonusEvent uabe = new UserAddBonusEvent(this);
      uabe.setUser(user);
      uabe.setAmount(amount);
      publisher.publishEvent(uabe);
   }
}