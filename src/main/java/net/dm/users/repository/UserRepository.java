package net.dm.users.repository;

import net.dm.users.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    public User findFirstById(@Param("id") long id);

    public User findFirstByEmail(@Param("email") String email);
}
