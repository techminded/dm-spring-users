package net.dm.users.repository;

import net.dm.users.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {

    public Role findFirstByName(@Param("name") String name);
}
