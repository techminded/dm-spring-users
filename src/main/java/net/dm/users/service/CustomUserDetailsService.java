package net.dm.users.service;

import net.dm.users.entity.CurrentUser;
import net.dm.users.entity.Role;
import net.dm.users.entity.User;
import net.dm.users.event.UserCreatedEventPublisher;
import net.dm.users.form.UserManageForm;
import net.dm.users.form.UserProfileForm;
import net.dm.users.form.UserRegisterForm;
import net.dm.users.repository.RoleRepository;
import net.dm.users.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ApplicationContext applicationContext;

    @Value("${dm.users.emailasusername}")
    private boolean emailAsUsername;

//    @Autowired
//    FinAccountService finAccountService;
//
//    @Autowired
//    FinTransactionService finTransactionService;

    @Override
    public CurrentUser loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findFirstByEmail(email);
        return new CurrentUser(user);
    }

    public User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;
        if (principal instanceof UserDetails) {
            user = ((CurrentUser)principal).getUser();
        }
        return user;
    }

    public User findOne(long id) {
        return userRepository.findOne(id);
    }

    public User findFirstById(long id) {
        return userRepository.findFirstById(id);
    }

    public User findFirstByEmail(String email) {
        return userRepository.findFirstByEmail(email);
    }

    public User save(User user) {
        if (emailAsUsername) {
            user.setUsername(user.getEmail());
        }
        user = userRepository.save(user);

        return user;
    }

    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public User updateFromManageForm(UserManageForm userManageForm, User user) {

        user.setUsername(userManageForm.getUsername());
        user.setEmail(userManageForm.getEmail());
        user.setEnabled(userManageForm.isEnabled());
        user.setBalanceBonus(userManageForm.getBalanceBonus());
        user.setPhone(userManageForm.getPhone());
        user.setCompany(userManageForm.getCompany());

        user = this.save(user);
        this.renewCurrentUserData(user);
        return user;
    }

    public User updateFromProfileForm(UserProfileForm userProfileForm, User user) {

        user.setUsername(userProfileForm.getUsername());
        user.setEmail(userProfileForm.getEmail());
        user.setPhone(userProfileForm.getPhone());
        user.setCompany(userProfileForm.getCompany());

        user = this.save(user);
        this.renewCurrentUserData(user);
        return user;
    }

    public User createFromRegisterForm(UserRegisterForm userRegisterForm) {
        User user = new User();
        user.setEmail(userRegisterForm.getEmail());
        user.setUsername(userRegisterForm.getUsername());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(userRegisterForm.getPassword());
        user.setPasswordHash(hashedPassword);
        Role userRole = roleRepository.findFirstByName("ROLE_USER");
        Set<Role> roles = new HashSet<Role>() {{
            add(userRole);
        }};
        user.setRoles(roles);
        user = userRepository.save(user);

        UserCreatedEventPublisher ucep = (UserCreatedEventPublisher) applicationContext.getBean("userCreatedEventPublisher");
        ucep.publish(user);
        return user;
    }

    public void delete(Long id) {
        userRepository.delete(id);
    }

    public void renewCurrentUserData(User user) {
        if (user == null) {
            String email = getCurrentUser().getEmail();
            user = findFirstByEmail(email);
        }
        if (emailAsUsername) {
            getCurrentUser().setUsername(user.getUsername());
        }
        getCurrentUser().setBalance(user.getBalance());
        getCurrentUser().setBalanceTotal(user.getBalanceTotal());
    }
}