package net.dm.users.controller;

import net.dm.users.entity.User;
import net.dm.users.event.UserAddBonusEventPublisher;
import net.dm.users.event.UserCreatedEventPublisher;
import net.dm.users.form.UserManageForm;
import net.dm.users.form.UserProfileForm;
import net.dm.users.form.UserRegisterForm;
//import net.dm.users.service.AppAccountService;
import net.dm.users.service.AuthService;
import net.dm.users.service.CustomUserDetailsService;

import net.hiwidget.service.MessageByLocaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class UserController {

    public static final String PANEL_MANAGE_PREFIX = "/panel/manage/user";

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    ApplicationContext applicationContext;

//    @Autowired
//    private AppAccountService appAccountService;

    @Autowired
    private AuthService authService;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    public String getPanelManagePrefix() {
        return PANEL_MANAGE_PREFIX;
    }

    @Value("${dm.users.emailasusername}")
    private boolean emailAsUsername;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("emailAsUsername", emailAsUsername);
        return "classpath:/templates/user/login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);

        return "redirect:/";
    }

    @RequestMapping(value = "/user/register", method = RequestMethod.GET)
    public String register(Model model) {
        UserRegisterForm userRegisterForm = new UserRegisterForm();

        model.addAttribute("user", userRegisterForm);
        model.addAttribute("emailAsUsername", emailAsUsername);
        return "classpath:/templates/user/register";
    }

    @RequestMapping(value = "/user/register", method = RequestMethod.POST)
    public String registerSubmit(HttpServletRequest request, @Valid @ModelAttribute("user") final UserRegisterForm userRegisterForm, BindingResult bindingResult, Model model) {
        model.addAttribute("emailAsUsername", emailAsUsername);
        if (emailAsUsername) {
            userRegisterForm.setUsername(userRegisterForm.getEmail());
        }
        if (bindingResult.hasErrors()) {
            return "classpath:/templates/user/register";
        } else {
            String email = userRegisterForm.getEmail();
            if (userService.findFirstByEmail(email) != null) {
                bindingResult.addError(new ObjectError("global",
                        messageByLocaleService.getMessage("user.registration.message.alreadyexists")));
                return "classpath:/templates/user/register";
            } else {
                User user = userService.createFromRegisterForm(userRegisterForm);

                authService.authenticate(request);
            }
        }

        return "redirect:/panel";
    }

    @RequestMapping(value = "/panel/profile", method = RequestMethod.GET)
    public String profile(Model model) {
        User user = userService.findOne(userService.getCurrentUser().getId());
        UserProfileForm userProfileForm = new UserProfileForm(user);
        //List<AppAccount> accounts = appAccountService.findByUser(user);

        model.addAttribute("user", userProfileForm);
        model.addAttribute("emailAsUsername", emailAsUsername);
        //model.addAttribute("accounts", accounts);
        return "classpath:/templates/user/profile";
    }

    @RequestMapping(value = "/panel/profile", method = RequestMethod.POST)
    public String profileSubmit(HttpServletRequest request, @ModelAttribute UserProfileForm userProfileForm, BindingResult bindingResult, Model model) {
        User user = userService.findOne(userService.getCurrentUser().getId());
        model.addAttribute("emailAsUsername", emailAsUsername);
        if (emailAsUsername) {
            userProfileForm.setUsername(userProfileForm.getEmail());
        }
        if (bindingResult.hasErrors()) {
            return "classpath:/templates/user/edit";
        } else {;
            user = userService.updateFromProfileForm(userProfileForm, user);
            if (!emailAsUsername) {
                user.setUsername(request.getParameter("username")); // username is saved with id
            }
            userService.save(user);
        }

        return "redirect:/panel/profile";
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = PANEL_MANAGE_PREFIX, method = RequestMethod.GET)
    public String manage(Model model) {
        Page<User> users = userService.findAll(new PageRequest(0, 20));

        model.addAttribute("users", users);
        return "classpath:/templates/user/manage";
    }

    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/create", method = RequestMethod.GET)
    public String create(Model model) {
        User user = new User();

        model.addAttribute("user", user);
        return "classpath:/templates/banner/edit";
    }

    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/create", method = RequestMethod.POST, consumes = "multipart/form-data")
    public String createSubmit(@ModelAttribute User user, Model model,
                               @RequestParam("imageFile") MultipartFile imageFile) {

        userService.save(user);
        return "redirect:" + PANEL_MANAGE_PREFIX;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") long id, Model model) {
        User user = userService.findOne(id);
        UserManageForm userManageForm = new UserManageForm(user);

        model.addAttribute("user", userManageForm);
        model.addAttribute("action", PANEL_MANAGE_PREFIX + "/edit/" + String.valueOf(user.getId()));
        return "classpath:/templates/user/edit";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/edit/{id}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public String editSubmit(@PathVariable("id") long id, @ModelAttribute UserManageForm userForm, BindingResult bindingResult, Model model) {
        User user = userService.findOne(id);
        if (bindingResult.hasErrors()) {
            return "classpath:/templates/user/edit";
        } else {;
            userService.updateFromManageForm(userForm, user);
        }
        return "redirect:" + PANEL_MANAGE_PREFIX;
    }

    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") long id, Model model) {

        User user = userService.findOne(id);

        model.addAttribute("user", user);
        return "classpath:/templates/user/delete";
    }

    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/delete/{id}", method = RequestMethod.POST)
    public String deleteSubmit(@PathVariable("id") long id) {
        userService.delete(id);

        return "redirect:/" + PANEL_MANAGE_PREFIX;
    }

    @RequestMapping(value = PANEL_MANAGE_PREFIX + "/addbonus", method = RequestMethod.POST,
            consumes="applicaion/json", produces = "application/json")
    @ResponseBody
    public String sendcode(@RequestParam("amount") int amount) {
        UserAddBonusEventPublisher uabep = (UserAddBonusEventPublisher) applicationContext.getBean("userAddBonusEventPublisher");
        uabep.publish(userService.getCurrentUser(), amount);
        return "{state: 'success'}";
    }
}
