package net.dm.users.form;


import net.dm.users.entity.User;

public class UserProfileForm {

    private long id;

    private String username;

    private String email;

    private String phone;

    private String company;

    public UserProfileForm() {
    }

    public UserProfileForm(User user) {
        this.setId(user.getId());
        this.setUsername(user.getUsername());
        this.setEmail(user.getEmail());
        this.setPhone(user.getPhone());
        this.setCompany(user.getCompany());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
