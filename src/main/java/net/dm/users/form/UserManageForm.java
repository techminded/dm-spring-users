package net.dm.users.form;


import net.dm.users.entity.User;

public class UserManageForm {

    private long id;

    private String username;

    private String email;

    private Boolean enabled;

    private Integer balanceBonus = 0;

    private String phone;

    private String company;

    public UserManageForm() {
    }

    public UserManageForm(User user) {
        this.setId(user.getId());
        this.setUsername(user.getUsername());
        this.setEmail(user.getEmail());
        this.setEnabled(user.isEnabled());
        this.setBalanceBonus(user.getBalanceBonus());
        this.setPhone(user.getPhone());
        this.setCompany(user.getCompany());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getBalanceBonus() {
        return balanceBonus;
    }

    public void setBalanceBonus(Integer balanceBonus) {
        this.balanceBonus = balanceBonus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
