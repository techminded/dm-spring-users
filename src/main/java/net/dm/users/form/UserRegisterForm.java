package net.dm.users.form;

import net.dm.util.constraints.FieldMatch;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@FieldMatch(first = "password", second = "confirmPassword", message = "{user.message.passwordmustmatch}")
public class UserRegisterForm {

    private String username;

    @NotNull
    @NotBlank(message = "This field is required")
    @Email
    private String email;

    private String password;

    private String confirmPassword;


    public UserRegisterForm() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
